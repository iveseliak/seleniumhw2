package com.gmail;



import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class TestGmail {
    WebDriver driver;

    @BeforeTest
    public void setURL(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver=new ChromeDriver();
        driver.get("https://www.gmail.com");
    }

    @Test
    public void gmailLoginTest(){
        driver.findElement(By.xpath("//*[@id='identifierId']")).sendKeys("test.test");
        driver.findElement(By.xpath("//div[@id='identifierNext']")).click();
        (new WebDriverWait(driver,30)).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input[type='password']")));
        driver.findElement(By.cssSelector("input[type='password']")).sendKeys("qwerty123");
        driver.findElement(By.xpath("//div[@id='passwordNext']")).click();
        String title="Gmail";
        String pageTitle=driver.getTitle();
        Assert.assertEquals(title,pageTitle);
    }

    @Test(dependsOnMethods = "gmailLoginTest")
    public void sendEmailTest(){
        WebElement element=null;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS) ;
        driver.findElement(By.xpath("//div[@class='z0']//div[@role='button']")).click();
        driver.findElement(By.xpath("//textarea[@class='vO' and @name='to']")).sendKeys("ivan-veseliak@ukr.net");
        driver.findElement(By.xpath("//input[@class='aoT' and @name='subjectbox']")).sendKeys("Test test test");
        driver.findElement(By.cssSelector("div[id=':hf']")).sendKeys("Test");
        element=driver.findElement(By.xpath("//div[@class='T-I J-J5-Ji aoO T-I-atl L3' and @role='button']"));
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("arguments[0].click();", element);
        String textExpected = "Надсилання…";
        String textActual = (new WebDriverWait(driver, 10)).until(msg->msg.findElement(By.xpath("//span[@class='bAq']")).getText());
        Assert.assertEquals( textExpected, textActual );
    }


    @AfterTest
    public  void quitDriver(){
        driver.quit();
    }

}
